CREATE DATABASE academia;

use academia;

CREATE TABLE socios (
    documento VARCHAR(32) NOT NULL,
    nombre VARCHAR(32) NOT NULL,
    residencia VARCHAR(64),
    PRIMARY KEY (documento)
);

CREATE TABLE inscritos (
    documento_socio VARCHAR(32) NOT NULL,
    asignatura VARCHAR(32) NOT NULL,
    año year NOT NULL,
    pagado enum ('s','n'),
    PRIMARY KEY (documento_socio,asignatura,año)
);

insert into socios values ('5059659Z','Pepe','Spain is different');
insert into socios values ('677778797','Juanito Valderrama','Segovia');
insert into socios values ('77789n','Carlitos Champion Torrijas','Torrijas City');

insert into inscritos values ('77789n','Guitarra segoviana','2019','s');
insert into inscritos values ('77789n','Cante segoviano','2019','s');
insert into inscritos values ('5059659Z','Pandereta','2019','n');
insert into inscritos values ('677778797','Pandereta','2019','s');
insert into inscritos values ('677778797','Zambomba clásica','2019','s');

select nombre, asignatura from
socios inner join inscritos
on socios.documento = inscritos.documento_socio;