-- import & export
/usr/local/mysql/bin/mysqldump -u root -p ventas > ventas.sql


select city.name, city.population, country.name 
from city inner join country on 
city.countrycode = country.code 
order by country.name desc;


select ciudad.name, ciudad.max_poblacion, country.name 
from country 
inner join (select Name, max(Population) as max_poblacion, countrycode from city group by Name,countrycode) as ciudad
on ciudad.countrycode = country.code;



select pais.name, idioma.Language, idioma.IsOfficial, idioma.Percentage
from country as pais
inner join countrylanguage as idioma
where pais.Code = idioma.CountryCode 
and Language like "%spanish%"
order by idioma.Percentage desc;

CREATE USER 'test'@'localhost' IDENTIFIED BY 'test';
GRANT ALL PRIVILEGES ON tienda.* TO 'test'@'localhost';
FLUSH PRIVILEGES;

-- listar usuarios
SELECT User FROM mysql.user;



-- Variables
--

select @pobmax:=max(population) from city where countryCode='ESP';
select Name from city where population > @pobmax;


-- 
-- Base de datos Tienda
--
CREATE DATABASE tienda;

use tienda;

CREATE TABLE IF NOT EXISTS productos (
    codigo VARCHAR(3) NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    precio DECIMAL(6,2) NOT NULL,
    fecha DATE NOT NULL,
    PRIMARY KEY (codigo)
);


INSERT INTO productos VALUES("c01","Cepillo abrillantar",2.50,"2017-11-02");
INSERT INTO productos VALUES("r01","Regleta mod. ZAZ",10,"2018-05-03");
INSERT INTO productos VALUES("r02","Regleta mod. XAX",15,"2018-05-03");
INSERT INTO productos VALUES("p01","Pegamento rápido",6.50,"2017-11-02");
INSERT INTO productos VALUES("p02","Pegamento industrial",14.50,"2017-10-06");
INSERT INTO productos VALUES("a03","Azada grande 60cm",50.60,"2018-05-03");
INSERT INTO productos VALUES("c02","Cepillo birutas",6.20,"2017-10-06");
INSERT INTO productos VALUES("c03","Cepillo jardín",22.35,"2018-05-03");
INSERT INTO productos VALUES("a01","Azada pequeña 30cm",25,"2017-10-06");
INSERT INTO productos VALUES("a02","Azada mediana 45 cm",37.50,"2017-11-02");

-- Añado columna
ALTER TABLE productos add categoria VARCHAR(32) after nombre;

INSERT INTO productos VALUES("z02","Cepillo Birutas pequeño","utensilios",6.30,"2019-04-06");

update productos set categoria = "utensilios";
update productos set categoria = "enchufes" where nombre like "regleta%";

-- Rename de la tabla y creo una tabla de backup
rename table productos to productosB;
create table productos_backup like productos;
insert productos_backup select * from productos;