CREATE DATABASE almacen;

use almacen;

CREATE TABLE fabricante (
    Codigo_Fab INT AUTO_INCREMENT,
    nombre VARCHAR(32) NOT NULL,
    PRIMARY KEY (Codigo_Fab)
);

CREATE TABLE IF NOT EXISTS articulos (
    Codigo INT AUTO_INCREMENT,
    nombre VARCHAR(128) NOT NULL,
    precio DECIMAL (6,2) NOT NULL,
    fabricante INT NOT NULL,
    PRIMARY KEY (Codigo),
    FOREIGN KEY (fabricante) REFERENCES fabricante(Codigo_Fab)
);

insert into fabricante values (1,'Bayer');
insert into fabricante values (2,'Laboratorios Juanito');
insert into fabricante values (3,'Compuestos Farmaceuticos SA');
insert into fabricante values (4,'Pfizer');


insert into articulos values (1,'Paracetamol de 1 gramo',1.50,1);
insert into articulos values (2,'Eparina',40,4);
insert into articulos values (3,'Medicina de la buena',25.5,2);
insert into articulos values (4,'Aspirina',2,1);
insert into articulos values (5,'Frenadol',6,3);
insert into articulos values (6,'Espara-trapo',2,2);
insert into articulos values (7,'Vitaminas',15,2);
insert into articulos values (8,'Vitamina C',12,1);
insert into articulos values (9,'Vitamina D ',5,4);
insert into articulos values (10,'Complejo vitaminico',24,3);
insert into articulos values (11,'Ibuprofeno',2,4);
insert into articulos values (12,'Vendas de 20 metros',7.14,3);
insert into articulos values (13,'Altavoz Bluetooth',70,2);



-- queries

select nombre, precio from articulos;

select nombre from articulos where precio < 3;

select nombre, precio from articulos where precio between 3 and 8;

select nombre, precio*166 as precio_pesetas from articulos;

select avg(precio) as media_precios from articulos;

select avg(precio) as media_precios from articulos where fabricante=2;

select count(*) from articulos where precio > 8;

select nombre, precio from articulos where precio between 3 and 8 order by precio desc,nombre asc;

select avg(precio), fabricante from articulos group by fabricante;

select @precio_minimo:=min(precio) from articulos;

select nombre, precio from articulos where precio = @precio_minimo;

update articulos set nombre = "Impresora Laser" where Codigo = 8;

update articulos set precio = precio*0.9;

update articulos set precio = precio - 3 where precio > 20;