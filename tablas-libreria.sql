--
-- Estructura de tablas BBDD Libreria
--

CREATE DATABASE libreria;

use libreria;

CREATE TABLE IF NOT EXISTS compradores (
    id_comprador INT AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    PRIMARY KEY (id_comprador)
);

CREATE TABLE IF NOT EXISTS editoriales (
    id_editorial INT AUTO_INCREMENT,
    nombre VARCHAR(125) NOT NULL,
    direccion VARCHAR(255) NOT NULL,
    telefono INT NOT NULL,
    PRIMARY KEY (id_editorial)
);

CREATE TABLE IF NOT EXISTS libros (
    id_libro INT AUTO_INCREMENT,
    titulo VARCHAR(255) NOT NULL,
    editorial INT NOT NULL,
    genero VARCHAR(25) NOT NULL ,
    isbn VARCHAR(50) NOT NULL,
    precio DECIMAL(3,2) NOT NULL,
    idioma VARCHAR(25) NOT NULL,
    fecha_publicacion INT NOT NULL,
    num_paginas INT NOT NULL,
    descripcion TEXT,
    PRIMARY KEY (id_libro),
    FOREIGN KEY (editorial) REFERENCES editoriales(id_editorial)
);

CREATE TABLE IF NOT EXISTS pedidos (
    id_pedido INT AUTO_INCREMENT,
    comprador INT NOT NULL,
    fecha DATE NOT NULL,
    PRIMARY KEY (id_pedido),
    FOREIGN KEY (comprador) REFERENCES compradores(id_comprador)
);

CREATE TABLE IF NOT EXISTS facturas (
    id_factura INT AUTO_INCREMENT,
    proveedor INT NOT NULL,
    fecha DATE NOT NULL,
    PRIMARY KEY (id_factura),
    FOREIGN KEY (proveedor) REFERENCES editoriales(id_editorial)
);

CREATE TABLE IF NOT EXISTS detalle_pedido (
    id_detalle INT AUTO_INCREMENT,
    pedido INT NOT NULL,
    libro  INT NOT NULL,
    cantidad TINYINT NOT NULL,
    PRIMARY KEY (id_detalle),
    FOREIGN KEY (pedido) REFERENCES pedidos(id_pedido),
    FOREIGN KEY (libro) REFERENCES libros(id_libro)
);

CREATE TABLE IF NOT EXISTS detalle_factura (
    id_det_fact INT AUTO_INCREMENT,
    factura INT NOT NULL,
    libro  INT NOT NULL,
    cantidad TINYINT NOT NULL,
    PRIMARY KEY (id_det_fact),
    FOREIGN KEY (factura) REFERENCES facturas(id_factura),
    FOREIGN KEY (libro) REFERENCES libros(id_libro)
);

ALTER TABLE libros ADD opiniones TINYINT NOT NULL AFTER idioma;
ALTER TABLE libros ADD autor VARCHAR(100) NOT NULL AFTER titulo;
ALTER TABLE pedidos ADD total INT NOT NULL AFTER fecha;
ALTER TABLE facturas ADD total INT NOT NULL AFTER fecha;
ALTER TABLE libros MODIFY PRECIO DECIMAL(5,2);
ALTER TABLE pedidos MODIFY total DECIMAL(7,2);