INSERT INTO compradores VALUES (1,'Juanito','Gonzalez');
INSERT INTO compradores VALUES (2,'Pablito','Clavo un clavito');
INSERT INTO compradores VALUES (3,'Pedro','Picapiedra');
INSERT INTO compradores VALUES (4,'Pablo','Picapiedra');
INSERT INTO compradores VALUES (5,'James','Bond');
INSERT INTO compradores VALUES (6,'Marta','Marta');
INSERT INTO compradores VALUES (7,'Mery','Mery');
INSERT INTO compradores VALUES (9,'Ramon','Ramirez');


INSERT INTO editoriales VALUES (1,'Anaya','Madrid Madrid',66666666);
INSERT INTO editoriales VALUES (2,'Proyectos Editoriales, Sa','Calle Basauri, 17, 28023, Madrid, Madrid',555555);
INSERT INTO editoriales VALUES (3,'Santillana','Avenida De Los Artesanos, 6, 28760, Tres Cantos Madrid, Madrid',66666666);
INSERT INTO editoriales VALUES (4,'Editorial Planeta','Barcelona Barcelona',00000000);

INSERT INTO libros VALUES (1,'Don Quijote de la Mancha',1,'Literatura Clásica','978-8420412146','013.20','Español',5,2008,1376,'Miguel de Cervantes (1547-1616) ejerció las más variadas profesiones antes de dedicarse plenamente a la literatura. Entró en Roma al servicio del cardenal Acquaviva, fue soldado en la batalla de Lepanto (1571), estuvo cinco años cautivo en Argel y ejerció como comisario real de abastos para la Armada Invencible. Tales oficios le reportaron una experiencia humana que supo plasmar magistralmente en todas sus obras. De su producción poética cabe destacar Viaje del Parnaso (1614), un verdadero testamento literario y espiritual. En el campo teatral cultivó la tragedia, la tragicomedia, la comedia y el entremés. Pero sin duda es en el terreno de la narrativa donde Cervantes se impuso a sus contemporáneos y obtuvo logros que le valdrían el título de creador de la novela moderna, con libros como La Galatea (1585), El ingenioso hidalgo don Quijote de la Mancha (1605), Novelas ejemplares (1613), El ingenioso caballero don Quijote de la Mancha (segunda parte de su obra cumbre, 1615) y, póstumamente, Los trabajos de Persiles y Sigismunda (1617).');
INSERT INTO libros VALUES (2,'Don Quijote de la Mancha',3,'Literatura Clásica','978-8467016901','15.75','Español',2,2016,1000,'Miguel de Cervantes Saavedra es una de las máximas figuras de la literatura clásica occidental, cultivó todos los géneros. Es autor de La Galatea, Novelas ejemplares, Los trabajos de Persiles y Segismunda, Viaje del Parnaso y Ocho comedias y ocho entremeses. El Quijote, su obra maestra, constituye una de las cimas de la literatura universal de todos los tiempos.');

INSERT INTO libros VALUES (3,'Harry Potter y la Piedra Filosofal: 1','J.K. Rowling',4,'Infantil','978-8478884452','14.25','Español',4,2015,245,'Harry Potter se ha quedado huérfano y vive en casa de sus abominables tíos y del insoportable primo Dudley. Harry se siente muy triste y solo, hasta que un buen día recibe una carta que cambiará su vida para siempre. En ella le comunican que ha sido aceptado como alumno en el colegio interno Hogwarts de magia y hechicería. A partir de ese momento, la suerte de Harry da un vuelco espectacular.');

INSERT INTO libros VALUES (4,'Breve historia del mundo (Divulgación. Historia)','Ernst H. Gombrich',2,'Historia','978-8499423470','8.25','Español',4,2019,325,'Esta Breve historia del mundo no es un manual al uso: es una epístola-diálogo escrita en un tono llano y accesible, sin los formalismos y las rigideces de los textos académicos, pero también sin el barniz de aparente pedagogía que da una voz de falsete a tantos libros infantiles y juveniles.
La perspectiva y el género elegidos por Gombrich convierten su libro en el antecesor moderno de otros tratados, como El mundo de Sofía de Jostein Gaarder o Ética para Amador de Fernando Savater, que, tras su apariencia de texto dirigido a los jóvenes, tocan cuestiones de hondo calado en el campo de las humanidades.
Una obra modernísima y de plena vigencia, de perspectiva amplia y profunda, de espectacular éxito en todo el mundo, que, por su optimismo, su amenidad y su sensibilidad, se lee como una novela.');



INSERT INTO pedidos VALUES (1,9,'2019-03-21','60.34');
INSERT INTO pedidos VALUES (2,2,'2019-03-22','18.20');
INSERT INTO pedidos VALUES (3,4,'2019-03-22','25.50');

INSERT INTO facturas VALUES (1,1,'2019-03-10','125.50');
INSERT INTO facturas VALUES (2,4,'2019-02-27','90.50');

INSERT INTO detalle_pedido VALUES (1,1,3,1);
INSERT INTO detalle_pedido VALUES (2,1,1,1);
INSERT INTO detalle_pedido VALUES (3,1,4,2);
INSERT INTO detalle_pedido VALUES (4,2,4,2);
INSERT INTO detalle_pedido VALUES (5,2,2,1);
INSERT INTO detalle_pedido VALUES (6,3,1,1);
INSERT INTO detalle_pedido VALUES (7,3,3,1);


INSERT INTO detalle_factura VALUES (1,1,2,3);
INSERT INTO detalle_factura VALUES (2,1,3,1);
INSERT INTO detalle_factura VALUES (3,2,4,10);
